FROM openjdk:11-slim-buster AS build

RUN mkdir -p /app
WORKDIR /app
copy . .
RUN chmod +x mvnw
RUN ./mvnw clean install package

# RUN
FROM openjdk:8-jdk-alpine

COPY --from=build /app/target/*.jar app.jar
EXPOSE "2222"
CMD ["java", "-jar", "app.jar"]

